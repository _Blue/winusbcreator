#include "Application.h"
#include "tools/utils.h"

#include <sstream>

Application::Application(HINSTANCE instance) :
  _window(instance, *this)
{
}

Application::~Application()
{
  if (_worker.joinable())
  {
    _worker.join();
  }
}

void Application::Run()
{
  _window.Log("WinUSBCreator 1.0");
  _window.Log("Source code available at: https://bitbucket.org/_Blue/winusbcreator");
  _window.Log("Build time: " + std::string(__DATE__));
  _window.Log("Blue, 2018\n");

  _window.Show();
}

void Application::OnImageSelected(const std::wstring& path)
{
  try
  {
    _image.emplace(path, *this);
    _window.SetImageDescriptions(_image->Content());
  }
  catch (const std::exception& e)
  {
    _window.Log(e.what());
  }
}

void Application::OnDestinationRefresh()
{
  std::vector<std::pair<std::string, std::string>> drives;

  for (const auto&e : utils::AvailableDrives())
  {
    if (e.substr(0, 2) == getenv("homedrive")) // let's avoid such an apocalypse
    {
      continue;
    }
    try
    {
      auto space = utils::DriveSpace(e);
      std::string title = utils::DriveTitle(e);

      std::ostringstream description;

      description << e << " ";
      if (!title.empty())
      {
        description << "(" << title << ") ";
      }
      description << "\t" << (space / (1024 * 1024)) << " MB";
      drives.emplace_back(e, description.str());
    }
    catch (const std::exception& error)
    {
      _window.Log("Failed to get drive infos for: " + e + ", " + error.what());
    }
  }

  _window.SetDevices(drives);
}

void Application::CreateImpl(size_t index, std::string target)
{
#ifndef _DEBUG
  try
  {
#endif
    std::string device = utils::GetDriveDevice(target);
    _window.Log("Detected drive device: " + device);
    _window.Log("Mark partition as active");
    utils::MarkPartitionActive(device);

    _window.Log("Apply image: destination: " + target + ", image index: " + std::to_string(index));
    _image->ApplyImage(index, utils::to_wstring(target));

    _window.Log("Install bootloader: destination: " + target);
    std::string cmd = "bcdboot.exe " + target + "windows /f all /s " + target;
    _window.Log("Execute: " + cmd);

    auto output = utils::CheckProcessOutput(cmd);

    _window.Log("Result: ", false);
    _window.Log(output.first, false);
    _window.Log(output.second, false);

    _window.Finished(true);
#ifndef _DEBUG
  }
  catch (const std::exception& e)
  {
    _window.Log(e.what());
    if (_continue_working) // If the user cancels, WIMApplyImage returns an error
    {
      _window.Finished(false);
    }
    else
    {
      _window.Cancelled();
    }
  }
#endif
}

void Application::OnCreate(size_t index, const std::string& path)
{
  if (_worker.joinable())
  {
    _worker.join();
  }
  _continue_working = true;
  _worker = std::thread{ std::bind(&Application::CreateImpl, this, index, path) };
}

bool Application::OnProcess(const std::string& file)
{
  return _continue_working;
}

bool Application::OnProgress(int progress)
{
  _window.SetProgress(progress);
  return _continue_working;
}

void Application::OnError(const std::string & message, int code)
{
  _window.Log("Error: " + message + ", " + std::to_string(code));
}

void Application::OnInfo(const std::string & message, int code)
{
  _window.Log(message + ", " + std::to_string(code));
}

void Application::OnRetry(const std::string & message, int code)
{
}

void Application::OnWarning(const std::string & message, int code)
{
  _window.Log("Warning: " + message + ", " + std::to_string(code));
}

void Application::OnCancel()
{
  _window.Log("Cancel");
  _continue_working = false;
}