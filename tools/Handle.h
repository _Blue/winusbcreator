#pragma once

// RAII compliant wrapper of Win32 handles

template <typename T>
class Handle
{
public:
  Handle(T val, bool wim_close = false);
  ~Handle();

  Handle(Handle<T>&& other);

  Handle(const Handle<T>&) = delete;
  const Handle& operator=(const Handle&) = delete;

  T Get() const;
  void Detach();
  void Close();
  operator T() const;
  bool Good() const;

private:
  T _value;
  bool _wim_close = false;
};

#include "handle.hxx"