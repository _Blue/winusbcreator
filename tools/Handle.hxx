#pragma once

#include <windef.h>
#include <type_traits>
#include <wimgapi.h>


static_assert(!std::is_same<HANDLE, SC_HANDLE>::value, "Insanity");

template <typename T>
inline Handle<T>::Handle(T hVal, bool _wim_close) :
  _value(hVal),
  _wim_close(_wim_close)
{
}

template <typename T>
inline Handle<T>::Handle(Handle<T>&& other) :
  _value(other._value),
  _wim_close(other._std_close)
{
  other.Detach();
}

template <typename T>
inline void Handle<T>::Close()
{
  static_assert(false, "Incorrect specialization of Handle::Close");
}

template <>
inline void Handle<HKEY>::Close()
{
  RegCloseKey(_value);
  Detach();
}

template <>
inline void Handle<HANDLE>::Close()
{
  if (_wim_close)
  {
    WIMCloseHandle(_value);
  }
  else
  {
    CloseHandle(_value);

  }
  Detach();
}

template <typename T>
inline Handle<T>::~Handle()
{
  if (Good())
  {
    Close();
  }
}

template <typename T>
inline Handle<T>::operator T() const
{
  return _value;
}

template <typename T>
inline T Handle<T>::Get() const
{
  return _value;
}

template <typename T>
inline void Handle<T>::Detach()
{
  _value = nullptr;
}

template <typename T>
inline bool Handle<T>::Good() const
{
  return _value != nullptr;
}