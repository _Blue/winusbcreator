#include <cassert>
#include <locale>
#include <codecvt>
#include <stdlib.h>
#include <io.h>
#include <fstream>
#include <windows.h>
#include <winioctl.h>
#include <Ntdddisk.h>

#include "utils.h"
#include "SyscallWrapper.h"
#include "handle.h"

using Converter = std::codecvt_utf8<wchar_t>;

static std::string DrivePath(char letter)
{
  char path[] = { letter, ':', '\\', '\0' };

  return path;
}

static std::pair<Handle<HANDLE>, Handle<HANDLE>> Pipe(bool rs, bool ws)
{
  HANDLE hread = nullptr;
  HANDLE hwrite = nullptr;
  SECURITY_ATTRIBUTES sa = { sizeof(SECURITY_ATTRIBUTES), nullptr, true };

  Syscall(CreatePipe, &hread, &hwrite, &sa, 0);

  if (rs)
  {
    Syscall(SetHandleInformation, hread, HANDLE_FLAG_INHERIT, 0);
  }
  if (ws)
  {
    Syscall(SetHandleInformation, hwrite, HANDLE_FLAG_INHERIT, 0);
  }

  return{ hread, hwrite };
}

static FILE* FileFromHandle(HANDLE file, const char* mode)
{
  int fd = SyscallWithError(_open_osfhandle, -1, reinterpret_cast<intptr_t>(file), 0);

  return Syscall(_fdopen, fd, mode);
}


// Because we need to execute bcdboot.exe, we have to disable the wow64 fs redirection
// (since bcdboot is not present in %windir%\syswow64)
//
// This is done using RAII to make sure that the context is restored even if an exception is thrown

class Wow64RedirHandler
{
public:
  Wow64RedirHandler()
  {
    Syscall(Wow64DisableWow64FsRedirection, &_value);
  }

  ~Wow64RedirHandler()
  {
    Syscall(Wow64RevertWow64FsRedirection, &_value);
  }

private:
  PVOID _value;
};

std::string utils::to_string(const std::wstring& input)
{
  std::wstring_convert<Converter> converter;

  return converter.to_bytes(input);
}

std::wstring utils::to_wstring(const std::string& input)
{
  std::wstring_convert<Converter> converter;

  return converter.from_bytes(input);
}

std::vector<std::string> utils::AvailableDrives()
{
  DWORD drives = GetLogicalDrives();

  std::vector<std::string> result;
  for (size_t i = 0; i < 32; i++)
  {
    if (drives & (1 << i))
    {
      std::string path = DrivePath(static_cast<char>('A' + i));

      DWORD type = GetDriveType(path.data());
      if (type & (DRIVE_REMOVABLE | DRIVE_FIXED))
      {
        result.push_back(std::move(path));
      }
    }
  }

  return result;
}

uint64_t utils::DriveSpace(const std::string& drive)
{
  Handle<HANDLE> device = Syscall(CreateFile,
    GetDriveDevice(drive).data(),
    GENERIC_READ,
    FILE_SHARE_READ | FILE_SHARE_WRITE,
    nullptr,
    OPEN_EXISTING,
    0,
    nullptr);

  GET_LENGTH_INFORMATION infos = { 0 };

  Syscall(DeviceIoControl,
    device.Get(),
    IOCTL_DISK_GET_LENGTH_INFO ,
    nullptr,
    0,
    &infos,
    sizeof(infos),
    nullptr,
    nullptr);
    

  return static_cast<uint64_t>(infos.Length.QuadPart);
}

std::string utils::DriveTitle(const std::string& drive)
{
  // https://msdn.microsoft.com/en-us/library/windows/desktop/aa364993(v=vs.85).aspx
  // The maximum buffer size is MAX_PATH+1.
  std::string name(MAX_PATH, '\0');

  Syscall(GetVolumeInformation,
    drive.data(),
    name.data(),
    name.size() + 1,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    0);

  name.resize(strlen(name.data()));
  return name;
}

std::pair<std::string, std::string> utils::CheckProcessOutput(const std::string & commandline)
{
  // Create the pipes
  auto stdout_pipe = Pipe(true, false);
  auto stderr_pipe = Pipe(true, false);

  // Create std streams out of Win Handles
  std::ifstream out_str(FileFromHandle(stdout_pipe.first.Get(), "r"));
  std::ifstream err_str(FileFromHandle(stderr_pipe.first.Get(), "r"));

  // Make sure handles won't be closed twice
  stdout_pipe.first.Detach();
  stderr_pipe.first.Detach();

  // Start process
  PROCESS_INFORMATION pi = { 0 };
  STARTUPINFOA si = { 0 };
  si.cb = sizeof(STARTUPINFO);
  si.hStdInput = GetStdHandle(STD_INPUT_HANDLE);
  si.hStdOutput = stdout_pipe.second;
  si.hStdError = stderr_pipe.second;
  si.wShowWindow = SW_HIDE;
  si.dwFlags = STARTF_USESTDHANDLES | STARTF_USESHOWWINDOW;

  // Disable Wow64 fs redirection (see details above)
  Wow64RedirHandler fs_handler;

  Syscall(CreateProcess,
    nullptr,
    const_cast<char*>(commandline.data()),
    nullptr,
    nullptr,
    true,
    0,
    nullptr,
    nullptr,
    &si,
    &pi);

  CloseHandle(pi.hThread);

  Handle<HANDLE> process(pi.hProcess);

  // Wait for process to finish
  WaitForSingleObject(process, INFINITE);

  // Verify exit code
  DWORD code = 0;
  Syscall(GetExitCodeProcess, process.Get(), &code);
  if (code != 0)
  {
    throw std::runtime_error("subprocess failed, " + std::to_string(code));
  }

  // Read stdout
  stdout_pipe.second.Close();
  std::string out(std::istreambuf_iterator<char>(out_str), {});

  // Read stderr
  stderr_pipe.second.Close();
  std::string err(std::istreambuf_iterator<char>(err_str), {});

  return { out, err };
}


std::string utils::GetDriveDevice(const std::string& path)
{
  assert(path.size() >= 2);
  return "\\\\.\\" + path.substr(0, 2);;
}


void utils::MarkPartitionActive(const std::string& name)
{
  // Open block device
  Handle<HANDLE> device = Syscall(CreateFile, 
    name.data(),
    FILE_ALL_ACCESS, 
    FILE_SHARE_READ | FILE_SHARE_WRITE, 
    nullptr, 
    OPEN_EXISTING, 
    0, 
    nullptr);

  // Create buffer for layout information (MBR can handle 4 partitions at most)
  std::vector<char> buffer(sizeof(DRIVE_LAYOUT_INFORMATION_EX) + sizeof(PARTITION_INFORMATION_EX) * 4, '\0');

  DRIVE_LAYOUT_INFORMATION_EX *infos = (DRIVE_LAYOUT_INFORMATION_EX*)buffer.data();
  ZeroMemory(infos, sizeof(infos));
  DWORD size = sizeof(infos);

  // Retrieve drive layout
  Syscall(DeviceIoControl,
    device.Get(),
    IOCTL_DISK_GET_DRIVE_LAYOUT_EX,
    nullptr,
    0,
    infos,
    buffer.size(),
    nullptr,
    nullptr);

  // Ignore if disk is not MBR
  if (infos->PartitionStyle != PARTITION_STYLE_MBR)
  {
    return;
  }

  assert(infos->PartitionCount > 0);

  // https://msdn.microsoft.com/en-us/library/aa364001.aspx
  //  On hard disks with the MBR layout, this value will always be a multiple of 4.

  if (infos->PartitionCount > 4 || infos->PartitionEntry[1].Mbr.PartitionType != PARTITION_ENTRY_UNUSED)
  {
    throw std::runtime_error("MBR Disk has more than one partition. That is not supported");
  }

  // Lock volume
  Syscall(DeviceIoControl,
    device.Get(),
    FSCTL_LOCK_VOLUME,
    nullptr,
    0,
    nullptr,
    0,
    nullptr,
    nullptr);

  // Set bootable flag on the partition
  infos->PartitionEntry[0].Mbr.BootIndicator = true;
  infos->PartitionEntry[0].RewritePartition = true;

  // Write new layout
  Syscall(DeviceIoControl,
    device.Get(),
    IOCTL_DISK_SET_DRIVE_LAYOUT_EX,
    infos,
    sizeof(DRIVE_LAYOUT_INFORMATION_EX) + sizeof(PARTITION_INFORMATION_EX) * infos->PartitionCount,
    nullptr,
    0,
    nullptr,
    nullptr);

  // Unlock device
  Syscall(DeviceIoControl,
    device.Get(),
    FSCTL_UNLOCK_VOLUME,
    nullptr,
    0,
    nullptr,
    0,
    nullptr,
    nullptr);
}