#pragma once

#include <type_traits>
#include "SyscallFailure.h"

template <typename Routine, typename ...Args>
typename std::result_of<Routine(Args...)>::type Syscall(Routine routine, Args... args);

template <typename Routine, typename ...Args>
typename std::result_of<Routine(Args...)>::type 
SyscallWithError(Routine routine, typename std::result_of<Routine(Args...)>::type error_value, Args... args);


#include "SyscallWrapper.hxx"
