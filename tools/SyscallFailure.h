#pragma once

#include <stdexcept>

class SyscallFailure : public std::exception
{
public:
  SyscallFailure(const std::string& message);

  const char* what() const noexcept override;
private:
  std::string _message;
};

