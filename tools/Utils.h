#pragma once

#include <string>
#include <vector>
#include <stdint.h>
namespace utils
{
  std::string to_string(const std::wstring& input);
  std::wstring to_wstring(const std::string& input);

  // Returns the list of connected drives that are:
  // - removable (USB)
  // - fixed (HDD)
  std::vector<std::string> AvailableDrives();

  // Returns (available_size, total_size)
  uint64_t DriveSpace(const std::string& drive);

  // Returns the volume string of the specified drive
  std::string DriveTitle(const std::string& drive);
  
  // Executes the command line and returns (stdout, stderr)
  std::pair<std::string, std::string> CheckProcessOutput(const std::string& commandline);

  void MarkPartitionActive(const std::string& device);
  std::string GetDriveDevice(const std::string& path);
}