#pragma once

#include <optional>
#include <thread>

#include "display/window.h"
#include "display/WindowEventHandler.h"
#include "image/WimMessageHandler.h"
#include "image/ImageHandler.h"
class Application : public WindowEventHandler, public WimMessageHandler
{
public:
  Application(HINSTANCE instance);
  ~Application();

  void Run();

private:
  void OnImageSelected(const std::wstring& path) override;
  void OnDestinationRefresh() override;
  void OnCreate(size_t index, const std::string& path) override;
  bool OnProcess(const std::string& file) override;
  bool OnProgress(int progress) override;
  void OnError(const std::string& message, int code) override;
  void OnInfo(const std::string& message, int code) override;
  void OnRetry(const std::string& message, int code) override;
  void OnWarning(const std::string& message, int code) override;
  void CreateImpl(size_t index, std::string destination); // Copy 'destination' to ensure liveness
  void OnCancel() override;
  
  bool _continue_working = true; // Set to false when user cancels
  Window _window;
  std::thread _worker;
  std::optional<ImageHandler> _image;
};