#include <Windows.h>
#include "Application.h"

int APIENTRY WinMain(HINSTANCE hinstance, HINSTANCE, LPSTR, int)
{
  Application app(hinstance);
  app.Run();

  return 0;
}