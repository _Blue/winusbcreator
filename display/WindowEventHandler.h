#pragma once

#include <string>

class WindowEventHandler
{
public:
  virtual void OnImageSelected(const std::wstring& path) = 0;
  virtual void OnDestinationRefresh() = 0;
  virtual void OnCreate(size_t index, const std::string& target) = 0;
  virtual void OnCancel() = 0;
};