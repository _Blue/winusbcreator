#pragma once

#include <Windows.h>
#include <vector>
#include <string>
#include <map>
#include <functional>
#include "WindowEventHandler.h"

class Window
{
public:
  Window(HINSTANCE instance, WindowEventHandler& handler);

  void Show();
  void SetProgress(int progress);
  void Finished(bool error);
  void SetImageDescriptions(const std::vector<std::map<std::string, std::string>>& content);
  void SetDevices(const std::vector<std::pair<std::string, std::string>>& devices);
  void Log(const std::string& input, bool lf = true);
  void Hide();
  void Cancelled();


private:
  bool OnEvent(UINT msg, WPARAM wparam, LPARAM lparam);
  static INT_PTR CALLBACK Callback(HWND window, UINT msg, WPARAM wparam, LPARAM lparam);
  void ProcessEvents();
  void OnSelectImage();
  void OnCreate();
  void InitImageList();
  void InitProgressBar();
  void UpdateCreateButton();
  void SetWorking(bool working);
  void OnQuit();
  HWND Item(int item);
  size_t SelectedImageIndex();
  std::string Destination();

  std::map<int, std::function<void()>> _command_handlers;
  std::vector<std::pair<std::string, std::string>> _destinations;
  WindowEventHandler& _handler;
  HWND _window = nullptr;
  HINSTANCE _hinstance = nullptr;
  bool _working = false;
};