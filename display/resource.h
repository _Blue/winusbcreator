//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by WindowsImageBuilder.rc
//
#define IDI_SMALL                       108
#define IDC_WINDOWSIMAGEBUILDER         109
#define IDD_WINDOW                      129
#define IDC_SELECT_IMAGE                1000
#define IDC_DESTINATION                 1002
#define IDC_START                       1004
#define IDC_SCAN                        1006
#define IDC_IMAGE_LIST                  1007
#define IDC_LOGS                        1008
#define IDC_IMAGE_NAME                  1009
#define IDC_PROGRESS                    1010
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1011
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
