#include <Windows.h>
#include <CommCtrl.h>
#include <cassert>
#include <shellapi.h>
#include <commdlg.h>
#include <windowsx.h>
#include "window.h"
#include "resource.h"

#pragma comment(linker, \
  "\"/manifestdependency:type='Win32' "\
  "name='Microsoft.Windows.Common-Controls' "\
  "version='6.0.0.0' "\
  "processorArchitecture='*' "\
  "publicKeyToken='6595b64144ccf1df' "\
  "language='*'\"")

static const std::vector<std::string> image_columns{ "index", "DISPLAYNAME", "TOTALBYTES", "FLAGS" };


Window::Window(HINSTANCE hinstance, WindowEventHandler& handler)
  : _handler(handler), _hinstance(hinstance)
{
  InitCommonControls();

  _command_handlers.emplace(IDC_SELECT_IMAGE, std::bind(&Window::OnSelectImage, this));
  _command_handlers.emplace(IDC_START, std::bind(&Window::OnCreate, this));
  _command_handlers.emplace(IDC_SCAN, std::bind(&WindowEventHandler::OnDestinationRefresh, &_handler));


  _window = CreateDialogParam(_hinstance,
    MAKEINTRESOURCE(IDD_WINDOW),
    nullptr,
    &Window::Callback,
    reinterpret_cast<LPARAM>(this));
}

void Window::Show()
{
  ShowWindow(_window, SW_SHOW);

  Log("Ready");
  ProcessEvents();
}

void Window::ProcessEvents()
{
  MSG msg = { 0 };
  while (GetMessage(&msg, nullptr, 0, 0))
  {
    if (!IsDialogMessage(_window, &msg))
    {
      TranslateMessage(&msg);
      DispatchMessage(&msg);
    }
  }
}

INT_PTR Window::Callback(HWND window, UINT msg, WPARAM wparam, LPARAM lparam)
{
  if (msg == WM_INITDIALOG)
  {
    Window* instance = reinterpret_cast<Window*>(lparam);
    instance->_window = window;
    SetWindowLongPtr(window, GWLP_USERDATA, lparam);
    instance->InitImageList();
    instance->InitProgressBar();
    instance->_handler.OnDestinationRefresh();
    return true;
  }

  auto ptr = GetWindowLongPtr(window, GWLP_USERDATA);
  Window* instance = reinterpret_cast<Window*>(ptr);

  if (instance == nullptr) // WM_INITDIALOG not received yet
  {
    return false;
  }

  return instance->OnEvent(msg, wparam, lparam);
}

bool Window::OnEvent(UINT msg, WPARAM wparam, LPARAM lparam)
{
  if (msg == WM_COMMAND)
  {
    if (HIWORD(wparam) == CBN_SELCHANGE)
    {
      UpdateCreateButton();
      return false;
    }
    auto routine = _command_handlers.find(LOWORD(wparam));
    if (routine == _command_handlers.end())
    {
      return false;
    }
    routine->second();
    return true;
  }
  else if (msg == WM_CLOSE)
  {
    OnQuit();
  }
  else if (msg == WM_NOTIFY)
  {
    NMHDR* notification = reinterpret_cast<NMHDR*>(lparam);

    if (notification->idFrom == IDC_IMAGE_LIST && notification->code == NM_CLICK)
    {
      UpdateCreateButton();
    }
  }

  return false;
}

void Window::OnQuit()
{
  if (_working)
  {
    if (MessageBox(_window, "Cancel the operation ?", "Cancel", MB_YESNO) == IDYES)
    {
      _handler.OnCancel();
    }
    return;
  }

  Hide();
}

void Window::Cancelled()
{
  SetWorking(false);
  Log("Ready");
}

void Window::Hide()
{
  EndDialog(_window, 0);
  PostQuitMessage(0);
}

void Window::UpdateCreateButton()
{
  Button_Enable(Item(IDC_START),
    ListView_GetNextItem(Item(IDC_IMAGE_LIST), -1, LVNI_SELECTED) != -1
    && ComboBox_GetCurSel(Item(IDC_DESTINATION)) != -1);
}

void Window::InitImageList()
{
  HWND list = Item(IDC_IMAGE_LIST);

  // Create the columns
  for (size_t i = 0; i < image_columns.size(); i++)
  {
    LVCOLUMN column = { 0 };
    column.mask = LVCF_TEXT;
    column.pszText = const_cast<char*>(image_columns[i].data());

    ListView_InsertColumn(list, i, &column);
  }

  // Adjust columns sizes
  ListView_SetColumnWidth(list, 0, 50);

  for (size_t i = 1; i < image_columns.size(); i++)
  {
    ListView_SetColumnWidth(list, i, LVSCW_AUTOSIZE_USEHEADER);
  }

  // Set full row select
  auto style = ListView_GetExtendedListViewStyle(list);
  ListView_SetExtendedListViewStyle(list, style | LVS_EX_FULLROWSELECT | LVS_EX_AUTOSIZECOLUMNS);
}

void Window::InitProgressBar()
{
  ShowWindow(Item(IDC_PROGRESS), SW_HIDE);
}

void Window::OnSelectImage()
{
  std::wstring path(MAX_PATH, '\0');

  OPENFILENAMEW input = { 0 };
  input.lStructSize = sizeof(input);
  input.hInstance = _hinstance;
  input.hwndOwner = _window;
  input.lpstrFile = const_cast<wchar_t*>(path.data());
  input.nMaxFile = MAX_PATH;
  input.lpstrTitle = L"Select an image";
  input.Flags = OFN_FILEMUSTEXIST | OFN_FORCESHOWHIDDEN | OFN_NONETWORKBUTTON | OFN_PATHMUSTEXIST;
  input.lpstrFilter = L"WIM image (*.wim)\0*.wim\0";

  if (!GetOpenFileNameW(&input))
  {
    return;
  }

  SetWindowTextW(Item(IDC_IMAGE_NAME), path.data());
  _handler.OnImageSelected(path);
}

void Window::SetImageDescriptions(const std::vector<std::map<std::string, std::string>>& content)
{
  HWND list = Item(IDC_IMAGE_LIST);

  ListView_DeleteAllItems(list);

  for (const auto &e : content)
  {
    LVITEM item = { 0 };
    item.mask = LVIF_TEXT;
    for (size_t i = 0; i < image_columns.size(); i++)
    {
      auto it = e.find(image_columns[i]);
      std::string text = (it == e.end()) ? "" : it->second;

      item.iSubItem = i;
      item.pszText = text.data();
      if (i == 0)
      {
        ListView_InsertItem(list, &item);
      }
      else
      {
        ListView_SetItem(list, &item);
      }
    }
  }

  // Adjust columns size
  for (size_t i = 1; i < image_columns.size(); i++)
  {
    ListView_SetColumnWidth(list, i, LVSCW_AUTOSIZE);
  }
}

void Window::SetDevices(const  std::vector<std::pair<std::string, std::string>>& devices)
{
  _destinations = devices;
  HWND list = Item(IDC_DESTINATION);

  ComboBox_ResetContent(list);

  for (const auto &e : devices)
  {
    ComboBox_AddString(list, e.second.data());
  }

  UpdateCreateButton();
}

void Window::Log(const std::string& input, bool lf)
{
  HWND console = Item(IDC_LOGS);
  int index = GetWindowTextLength(console);

  HWND focus = GetFocus();
  SendMessage(console, EM_SETSEL, index, index);
  SendMessage(console, EM_REPLACESEL, 0, reinterpret_cast<LPARAM>(input.data()));
  SetFocus(focus);

  if (lf)
  {
    Log("\r\n", false);
  }
}

size_t Window::SelectedImageIndex()
{
  HWND list = Item(IDC_IMAGE_LIST);
  int selected = ListView_GetNextItem(list, -1, LVNI_SELECTED);
  assert(selected != -1);

  std::string item(10, '\0');

  ListView_GetItemText(list, selected, 0, item.data(), item.size());

  item.resize(strlen(item.data()));
  assert(!item.empty());

  return std::stoi(item);
}

HWND Window::Item(int item)
{
  assert(_window != nullptr);
  return GetDlgItem(_window, item);
}

std::string Window::Destination()
{
  HWND combo = Item(IDC_DESTINATION);
  int index = ComboBox_GetCurSel(combo);
  assert(index != -1 && index < _destinations.size());

  return _destinations[index].first;
}

void Window::SetProgress(int progress)
{
  SendMessage(Item(IDC_PROGRESS), PBM_SETPOS, progress, 0);
}

void Window::Finished(bool success)
{
  SetWorking(false);
  if (!success)
  {
    MessageBox(_window, "An error occured, please check the logs for more details", "Sorry", MB_OK | MB_ICONERROR);
    return;
  }
  Log("Done!");
}

void Window::SetWorking(bool working)
{
  ShowWindow(Item(IDC_START), working ? SW_HIDE : SW_SHOW);
  ShowWindow(Item(IDC_PROGRESS), working ? SW_SHOW : SW_HIDE);
  EnableWindow(Item(IDC_IMAGE_LIST), !working);
  EnableWindow(Item(IDC_SCAN), !working);
  EnableWindow(Item(IDC_SELECT_IMAGE), !working);
  EnableWindow(Item(IDC_DESTINATION), !working);

  _working = working;
}

void Window::OnCreate()
{
  SetProgress(0);
  SetWorking(true);
  _handler.OnCreate(SelectedImageIndex(), Destination());
}