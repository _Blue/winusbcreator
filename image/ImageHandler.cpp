#include <Windows.h>
#include <wimgapi.h>
#include <cassert>
#include <filesystem>
#include <pugixml.hpp>
#include "ImageHandler.h"
#include "tools/SyscallWrapper.h"
#include "tools/Utils.h"

ImageHandler::ImageHandler(const std::wstring& path, WimMessageHandler& handler) :
  _image(OpenImage(path), true),
  _handler(handler)
{
  // WIMSetTemporaryPath must be called before doing any operation on the image
  SetTempPath();
  SetCallback();
}

ImageHandler::~ImageHandler()
{
  UnsetCallback();
  std::experimental::filesystem::remove(_work_folder);
}

ImageHandler::ImageContent ImageHandler::Content() const
{
  wchar_t* xml = nullptr;
  DWORD size = 0;

  Syscall(WIMGetImageInformation, _image.Get(), reinterpret_cast<void**>(&xml), &size);

  auto content = ParseContent(xml, size);
  LocalFree(xml);

  return content;
}

HANDLE ImageHandler::OpenImage(const std::wstring& path)
{
  return Syscall(&WIMCreateFile, path.c_str(),
    WIM_GENERIC_READ,
    WIM_OPEN_EXISTING,
    0,
    0,
    nullptr);
}

DWORD ImageHandler::ApplyCallback(DWORD message, WPARAM wparam, LPARAM lparam, ImageHandler* instance)
{
  auto translate = [](bool value)
  {
    return value ? WIM_MSG_SUCCESS : WIM_MSG_ABORT_IMAGE;
  };
  switch (message)
  {
  case WIM_MSG_PROGRESS:
    return translate(instance->_handler.OnProgress(static_cast<int>(wparam)));
  case WIM_MSG_PROCESS:
    return translate(instance->_handler.OnProcess(utils::to_string(reinterpret_cast<wchar_t*>(wparam))));
  case WIM_MSG_ERROR:
    instance->_handler.OnError(utils::to_string(reinterpret_cast<wchar_t*>(wparam)), static_cast<int>(lparam));
    return WIM_MSG_SUCCESS;
  case WIM_MSG_RETRY:
    instance->_handler.OnRetry(utils::to_string(reinterpret_cast<wchar_t*>(wparam)), static_cast<int>(lparam));
    return WIM_MSG_SUCCESS;
  case WIM_MSG_INFO:
    instance->_handler.OnInfo(utils::to_string(reinterpret_cast<wchar_t*>(wparam)), static_cast<int>(lparam));
    return WIM_MSG_SUCCESS;
  case WIM_MSG_WARNING:
    instance->_handler.OnWarning(utils::to_string(reinterpret_cast<wchar_t*>(wparam)), static_cast<int>(lparam));
    return WIM_MSG_SUCCESS;
  }

  return WIM_MSG_SUCCESS;
}

void ImageHandler::SetTempPath()
{
  _work_folder = std::experimental::filesystem::temp_directory_path().string() + "\\win_image_builder";
  std::experimental::filesystem::create_directories(_work_folder);

  Syscall(WIMSetTemporaryPath, _image.Get(), utils::to_wstring(_work_folder).data());
}

void ImageHandler::SetCallback()
{
  SyscallWithError(WIMRegisterMessageCallback,
    INVALID_CALLBACK_VALUE,
    _image.Get(),
    reinterpret_cast<FARPROC>(&ImageHandler::ApplyCallback),
    this);
}

void ImageHandler::UnsetCallback()
{
  Syscall(WIMUnregisterMessageCallback,
    _image.Get(),
    reinterpret_cast<FARPROC>(&ImageHandler::ApplyCallback));
}

void ImageHandler::ApplyImage(size_t index, const std::wstring& path)
{
  assert(index >= 1);
  Handle<HANDLE> image = { Syscall(WIMLoadImage, _image.Get(), index), true };

  Syscall(WIMApplyImage, image.Get(), path.c_str(), 0);
}

ImageHandler::ImageContent ImageHandler::ParseContent(const wchar_t* content, size_t len)
{
  pugi::xml_document node;
  node.load_buffer(content, len);

  std::vector<std::map<std::string, std::string>> result;

  auto read_info = [](const pugi::xml_node &node)
  {
    std::map<std::string, std::string> result;
    result.emplace("index", node.attribute("INDEX").as_string());

    for (const auto &e : node.children())
    {
      if (!e.empty() && e.first_child().type() == pugi::node_pcdata)
      {
        result.emplace(e.name(), e.child_value());
      }
    }
    return result;
  };

  auto root = node.first_child();
  assert(root.begin() != root.end());


  std::transform(++root.begin(), root.end(), std::back_inserter(result), read_info);

  return result;
}