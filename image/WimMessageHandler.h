#pragma once
#pragma once

#include <string>

class WimMessageHandler
{
public:
  virtual bool OnProcess(const std::string& file) = 0;
  virtual bool OnProgress(int progress) = 0;
  virtual void OnError(const std::string& message, int code) = 0;
  virtual void OnInfo(const std::string& message, int code) = 0;
  virtual void OnRetry(const std::string& message, int code) = 0;
  virtual void OnWarning(const std::string& message, int code) = 0;
};