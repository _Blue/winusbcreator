#pragma once

#include <string>
#include <vector>
#include <map>
#include "tools/handle.h"
#include "WimMessageHandler.h"

class ImageHandler
{
public:
  using ImageContent = std::vector<std::map<std::string, std::string>>;
  ImageHandler(const std::wstring& path, WimMessageHandler& handler);
  ~ImageHandler();

  ImageContent Content() const;
  void ApplyImage(size_t index, const std::wstring& path);

private:
  static HANDLE OpenImage(const std::wstring& path);
  static DWORD CALLBACK ApplyCallback(DWORD message, WPARAM wparam, LPARAM lparam, ImageHandler* instance);
  static ImageContent ParseContent(const wchar_t* content, size_t len);

  void SetTempPath();
  void SetCallback();
  void UnsetCallback();

  Handle<HANDLE> _image;
  std::string _work_folder;
  WimMessageHandler& _handler;
};